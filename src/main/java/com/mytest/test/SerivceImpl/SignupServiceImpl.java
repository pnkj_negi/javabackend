package com.mytest.test.SerivceImpl;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mytest.test.Repository.SignupRepository;
import com.mytest.test.Service.SignupService;
import com.mytest.test.entity.Domain;
import com.mytest.test.entity.SignupEntity;
import com.mytet.test.util.CommonUtil;
import com.mytet.test.util.EncryptionUtil;

@Service
public class SignupServiceImpl implements SignupService{

	@Autowired
	SignupRepository signup;
	
	@Override
	public Domain<String> addUser(SignupEntity bean) {
		Domain<String> s = new Domain<String>();
		try {
			
			bean.setPassword(EncryptionUtil.encryptPropertyValue(bean.getPassword()));
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidAlgorithmParameterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(signup.isUserExist(bean.getEmail())) {
			s.setMessage("Email already exist.");
			s.setHasError(true);
		} else {
			signup.addUser(bean);
			s.setMessage("Data saved successfully.");
			s.setHasError(false);			
		}
		
	
		return s;
	}

	@Override
	public Domain<SignupEntity> getuserByEmail(String email, String password) {
		SignupEntity user = new SignupEntity();
		Domain<SignupEntity> response = new Domain<SignupEntity>();
		user = signup.getuserByEmail(email);
		if (CommonUtil.isNull(user.getEmail())) {
			response.setMessage("User Id or password is invalid");
			response.setHasError(true);
		} else {
			String decryptedPass = null;
			try {
				decryptedPass = EncryptionUtil.decryptPropertyValue(user.getPassword());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (decryptedPass.equals(password)) {
	
				response.setMessage("Login is successful.");
				response.setHasError(false);
			} else {
				response.setMessage("User Id or password is invalid.");
				response.setHasError(true);
			}
		}
		return response;
	}
	
	

}
