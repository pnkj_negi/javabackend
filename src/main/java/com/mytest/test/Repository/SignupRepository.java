package com.mytest.test.Repository;

import com.mytest.test.entity.SignupEntity;

public interface SignupRepository {
	
	public Boolean addUser(SignupEntity bean);
	public SignupEntity getuserByEmail(String emailId);
	public Boolean isUserExist(String email);

}
