package com.mytest.test.RepositoryImpl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.mytest.test.Repository.SignupRepository;
import com.mytest.test.entity.SignupEntity;
import com.mytet.test.util.CommonUtil;

@Repository
public class SignupRepositoryImpl implements SignupRepository{
	
	@PersistenceContext
	private EntityManager entityManager;

	protected EntityManager getEntityManager() {
		return entityManager;
	}

	@Override
	@Transactional
	public Boolean addUser(SignupEntity bean) {
		
        Boolean flag = false;
		getEntityManager().persist(bean);
		flag = true;

		return flag;
	}

	@Override
	public SignupEntity getuserByEmail(String emailId) {
		SignupEntity bean = new SignupEntity();
		String sqlQuery = "Select * from Signup where email = :emailId";
		Query query = null;
		try {
			try {
				query = getEntityManager().createNativeQuery(sqlQuery, SignupEntity.class);
				query.setParameter("emailId", emailId);
				bean = (SignupEntity) query.getSingleResult();

			} catch (Exception e) {
				e.printStackTrace();
			}
			return bean;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Boolean isUserExist(String email) {
		Boolean found = false;
		String sqlQuery = "Select * from Signup where email = :email";
		Query query = null;
		try {
			try {
				SignupEntity bean = new SignupEntity();
				query = getEntityManager().createNativeQuery(sqlQuery, SignupEntity.class);
				query.setParameter("email", email);
				bean = (SignupEntity) query.getSingleResult();

				if (!CommonUtil.isNull(bean.getEmail())) {
					found = true;
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			return found;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return found;
	}

}
