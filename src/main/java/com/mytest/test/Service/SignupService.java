package com.mytest.test.Service;

import com.mytest.test.entity.Domain;
import com.mytest.test.entity.SignupEntity;

public interface SignupService {
	
	public Domain<String> addUser(SignupEntity bean);
	public Domain<SignupEntity> getuserByEmail(String email,String password);

}
