package com.mytest.test.entity;


public class Domain<K>{
	private K object;
	private Boolean hasError;
	private String message;
	
	public K getObject() {
		return object;
	}

	public void setObject(K object) {
		this.object = object;
	}

	public String getMessage() {
		return message;
	}

	public Boolean getHasError() {
		return hasError;
	}

	public void setHasError(Boolean hasError) {
		this.hasError = hasError;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	
}
