package com.mytest.test.entity;

public class ResponseObject {
	
	private String status;
	private Object object;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Object getObject() {
		return object;
	}
	public void setObject(Object object) {
		this.object = object;
	}
	@Override
	public String toString() {
		return "ResponseObject [status=" + status + ", object=" + object + "]";
	}
	
	
	

}
