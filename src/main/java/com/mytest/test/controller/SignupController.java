package com.mytest.test.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mytest.test.Service.SignupService;
import com.mytest.test.entity.Domain;
import com.mytest.test.entity.SignupEntity;

@Controller
@RequestMapping(path= "/login")
public class SignupController {
	
	@Autowired
	SignupService signup;
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Domain<String>> createUser(@RequestBody SignupEntity user) {
		Domain<String> users= signup.addUser(user);
		return new ResponseEntity<Domain<String>>(users,HttpStatus.OK);
	
	}
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/authenticatebyemail", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Domain<SignupEntity>> getuserByEmail(@RequestParam("email") String email,
			@RequestParam("password") String password){
		System.out.println("email:"+email);
		Domain<SignupEntity> entity = signup.getuserByEmail(email,password);
		return new ResponseEntity<Domain<SignupEntity>>(entity,HttpStatus.OK);
	}

}
